const mongoose = require('mongoose');
const config = require('./config');

const Photo = require('./models/Photo');
const User = require('./models/User');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('photos');
    await db.dropCollection('users');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [user1, user2] = await User.create({
    username: 'user',
    password: '123'
  }, {
    username: 'user1',
    password: '123'
  });


  await Photo.create({
    title: 'Great mountains',
    user: user1._id,
    image: 'forest.jpg'
  }, {
    title: 'Beautiful sea',
    user: user2._id,
    image: 'forest.jpg'
  }, {
    title: 'Amazing forest',
    user: user2._id,
    image: 'forest.jpg'
  });

  db.close();
});