const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Photo = require('../models/Photo');

const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
  // photo index
  router.get('/', (req, res) => {
    Photo.find().populate('user')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  // photo create
  router.post('/', [auth, upload.single('image')], (req, res) => {
    const photoData = req.body;

    if (req.file) {
      photoData.image = req.file.filename;
    } else {
      photoData.image = null;
    }
    photoData.user = req.user._id;
    const photo = new Photo(photoData);
    photo.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  router.get('/user/:id', (req, res) => {
    if (req.params.id) {
      Photo.find({user:req.params.id})
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    } else {
      Photo.find()
        .then(results => res.send(results))
        .catch(() => res.sendStatus(500));
    }

    router.delete('/user/:id', auth, async (req, res) => {
      const photo = await Photo.findOneAndRemove({_id: req.params.id});
      res.send(photo)
    });


  });

  return router;
};

module.exports = createRouter;