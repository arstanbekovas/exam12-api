const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPath: path.join(rootPath, '/public/uploads'),
  db: {
    url: 'mongodb://localhost:27017',
    name: 'photo'
  },
  jwt: {
    secret: 'secret',
    expires: '7d'
  },
  facebook: {
    appId: "1955882144444968", // Enter your app ID here
    appSecret: "98ef46b7839666d13fe8d7e2b64931a8" // Enter your app secret here
  }
};

